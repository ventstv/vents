$(function() {

	$('.menu-button').click(function(e) {
		var $container = $(".sandwich-mnu");

		if ($container.css('display') != 'block') {
			$container.show();

			var firstClick = true;
			$(document).bind('click.myEvent', function(e) {
				if (!firstClick && $(e.target).closest('.sandwich-mnu').length == 0) {
					$container.hide();
					$(document).unbind('click.myEvent');
				}
				firstClick = false;
			});
		}
		e.preventDefault();

	});

	$(".published-item").slice(0, 3).show();
	$(".load-more").on('click', function (e) {
		e.preventDefault();
		$(".published-item:hidden").slice(0, 3).slideDown();
		if ($(".published-item:hidden").length === 0) {
			$("#load").fadeOut('slow');
		}
		$('html,body').animate({
			scrollTop: $(this).offset().top-130
		}, 1300);
	});
	
	$('.filter-grid').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3
			}
		},
		{
			breakpoint: 992,
			settings: {
				slidesToShow: 2
			}
		},
		{
			breakpoint: 640,
			settings: {
				slidesToShow: 1
			}
		}
		]
	});

	$('.lang-selected').click(function(e) {
		var $container = $(".languages");

		if ($container.css('display') != 'block') {
			$container.slideDown();

			var firstClick = true;
			$(document).bind('click.myEvent', function(e) {
				if (!firstClick && $(e.target).closest('.languages').length == 0) {
					$container.slideUp();
					$(document).unbind('click.myEvent');
				}
				firstClick = false;
			});
		}

		e.preventDefault();
	});

	$('.dropdown-toggle').dropdown();
	$('.dropdown-menu input, .dropdown-menu label').click(function(e) {
		e.stopPropagation();
	});

	$('.rate').raty();

});

var filtered = false;

$('.filter-button').on('click', function(){
	var filtername = $(this).attr('id');
	var currentclass = $(this).attr('class');
	if (filtered === false) {
		$('.filter-grid').slick('slickUnfilter');
		$('.filter-grid').slick('slickFilter','.filter-' + filtername);
		$('.filter-button').attr('class', 'filter-button');
		$(this).addClass('active');
	} else {
		$('.filter-grid').slick('slickUnfilter');
		$('.filter-grid').slick('slickFilter', '.filter-' + filtername);
		$('.filter-grid').slickGoTo(0);
		$('.filter-button').attr('class', 'filter-button');
		$(this).removeClass('active');
		filtered = false;
	}
});